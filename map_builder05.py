import folium

def style_function(feature):
    employed = employed_series.get(int(feature["id"][-5:]), None)
    return {
        "fillOpacity": 0.5,
        "weight": 0,
        "fillColor": "#black" if employed is None else colorscale(employed),
    }


m=folium.Map(location=[-33,-70], zoom_start=8)
folium.GeoJson('https://bitbucket.org/quantcl/aguas/raw/57f7914c8060e410f21cb6153ecbfb84cef50da2/JSON/Aeropuertos05.json', name='aeropuertos').add_to(m)
folium.GeoJson('https://bitbucket.org/quantcl/aguas/raw/57f7914c8060e410f21cb6153ecbfb84cef50da2/JSON/Areas_Pobladas05.json', name='areas pobladas').add_to(m) # use style
folium.GeoJson('https://bitbucket.org/quantcl/aguas/raw/57f7914c8060e410f21cb6153ecbfb84cef50da2/JSON/Areas_Silvestres05.json', name='areas silvestres').add_to(m) # use style
folium.LayerControl().add_to(m)
m.save('tres_leches05.html')
