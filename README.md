# Contenido #

Este repositorio pretendo recopilar fuentes de información geográfica asociada a las aguas de Chile

### Qué queremos incluir? ###

* Hidrografía: ríos, lagos, humedales, cuencas!
* Pesca, centrales hidroeléctricas
* Geografía normativa? Comunas, Provincias, límites Regionales

[Biblioteca Nacional del Congreso](https://www.bcn.cl/siit/mapas_vectoriales)

### Humedales ###

* [Inventario 2020](https://humedaleschile.mma.gob.cl/inventario-humadales/)

Sergio Lucero